<?php

abstract class AbstractDAL
{
    private function __construct()
    {
        
    }

    protected static function normalizeFilters(array $schema, $filters) : array
    {
        return array_merge(array_fill_keys($schema, null), $filters);
    }
}
