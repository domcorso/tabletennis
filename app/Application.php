<?php

class Application
{
    private $navigationVisible = true;

    public function __construct()
    {

    }

    public function login($username, $password) : bool
    {
        $actual_username = $_ENV["APP_USERNAME"];
        $actual_password = $_ENV["APP_PASSWORD"];

        if ($username === $actual_username && $password === $actual_password) {
            $_SESSION["logged_in"] = true;
            return true;
        } return false;
    }

    public function logout()
    {
        $_SESSION = [];
        session_destroy();
    }

    public function isLoggedIn() : bool
    {
        return isset($_SESSION["logged_in"]) && $_SESSION["logged_in"] === true;
    }

    public function requiresLogin(bool $isApiEndpoint = false)
    {
        if ($this->isLoggedIn()) {
            return;
        }

        http_response_code(401);

        if ($isApiEndpoint) {
            die();
        }
        
        if (!$this->isLoggedIn()) {
            Helper::redirectTo("login");
        }
    }

    public function dieWith(int $responseCode) {
        http_response_code($responseCode);
        die();
    }

    public function setNavigationVisibility(bool $value)
    {
        $this->navigationVisible = $value;
    }

    public function getNavigationVisibility() : bool
    {
        return $this->navigationVisible;
    }

    public function getDefaultPage() : string
    {
        return "dashboard";
    }

    /**
     * Generates a link for a public file resource. Used for
     * cache busting files (i.e. CSS, JS etc.)
     */
    public function resource(string $publicRelativePath) : string {
        $path = realpath(__DIR__ . "/../public/" . $publicRelativePath);
        $lastModified = filemtime($path);
        $hash = hash("crc32", $lastModified);

        return $publicRelativePath . "?v=" . $hash;
    }
}
