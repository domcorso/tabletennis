<?php

class Database
{
    static $pdo = null;

    private function __construct()
    {
        
    }

    public static function getPDO() : PDO
    {
        if (self::$pdo === null) {
            self::connect();
        } return self::$pdo;
    }

    private static function connect()
    {
        $db_host = $_ENV["DB_HOST"];
        $db_name = $_ENV["DB_NAME"];
        $db_user = $_ENV["DB_USER"];
        $db_password = $_ENV["DB_PASSWORD"];
        $dsn = "mysql:dbname={$db_name};host={$db_host}";

        try {
            self::$pdo = new PDO($dsn, $db_user, $db_password, []);
            self::$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die("Could not connect to database.");
        }
    }
}
