<?php

class Game
{
    private $id = null;
    private $scoreboard = null;
    private $createdUtc = null;

    public function __construct()
    {
        $this->scoreboard = new Scoreboard();
    }

    public static function fromDataSource(array $data) : Game
    {
        $game = new Game();

        $game->setId((int) $data["game_id"]);
        $game->setCreatedUtc(new DateTime($data["created_utc"], new DateTimeZone("UTC")));
        $game->setScoreboard(GameDAL::getScoreboard($game));

        return $game;
    }

    protected function setId(int $id) : Game
    {
        $this->id = $id;

        return $this;
    }

    protected function setCreatedUtc(DateTime $createdUtc) : Game
    {
        $this->createdUtc = $createdUtc;

        return $this;
    }

    protected function setScoreboard(Scoreboard $scoreboard) : Game
    {
        $this->scoreboard = $scoreboard;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getScoreboard()
    {
        return $this->scoreboard;
    }

    public function getPlayers()
    {
        return $this->getScoreboard()->getPlayers();
    }

    public function getCreatedUtc()
    {
        return $this->createdUtc;
    }

    public function __toString() : string
    {
        $str = "";
        $str .= Helper::toLocal($this->createdUtc)->format("r") . " | ";
        
        foreach ($this->getScoreboard()->getAllScores() as $pair) {
            $str .= $pair->player->getName() . " (" . $pair->score . ") ";
        }

        return $str;
    }
}
