<?php

class GameDAL extends AbstractDAL
{
    public static function getGames(array $filters = []) : array
    {
        $statement = Database::getPDO()->prepare("
            SELECT g.game_id
                , g.created_utc
            FROM game g
            WHERE 1 = 1
                AND (:game_id IS NULL OR :game_id = g.game_id)
            ORDER BY g.created_utc DESC
        ");

        $games = [];
        $statement->execute(parent::normalizeFilters([
            "game_id"
        ], $filters));

        while (($row = $statement->fetch()) !== false) {
            $games[] = Game::fromDataSource($row);
        }

        return $games;
    }

    public static function getGameById(int $gameId)
    {
        $games = self::getGames([
            "game_id" => $gameId
        ]);

        if (count($games) === 1) {
            return $games[0];
        }

        return null;
    }

    public static function getScoreboard(Game $game) : Scoreboard
    {
        $scoreboard = new Scoreboard();

        $statement = Database::getPDO()->prepare("
            SELECT gp.player_id
                , gp.score
            FROM game_player gp
            WHERE gp.game_id = :game_id
        ");

        $statement->execute([
            "game_id" => $game->getId()
        ]);

        while (($scoreRow = $statement->fetch()) !== false) {
            $player = PlayerDAL::getPlayerById($scoreRow["player_id"]);
            $scoreboard->addScore($player, (int) $scoreRow["score"]);
        }

        return $scoreboard;
    }

    public static function getPlayers(Game $game) : array
    {
        if ($game->getId() === null) {
            return [];
        }

        $statement = Database::getPDO()->prepare("
            SELECT p.player_id
                , p.name
                , p.color
                , p.created_utc
            FROM player p
            INNER JOIN game_player gp ON p.player_id = gp.player_id
            WHERE gp.game_id = :game_id
            ORDER BY p.name ASC
        ");

        $players = [];
        $statement->execute([
            "game_id" => $game->getId()
        ]);

        while (($row = $statement->fetch()) !== false) {
            $players[] = Player::fromDataSource($row);
        }

        return $players;
    }

    public static function insert(Game $game) : bool
    {
        if ($game->getId() !== null || count($game->getScoreboard()->getPlayers()) !== 2) {
            return false;
        }

        $gameStmt = Database::getPDO()->prepare("
            INSERT INTO game (
                created_utc
            )
            VALUES (
                UTC_TIMESTAMP()
            )
        ");

        $gameStmt->execute();
        $newGameId = Database::getPDO()->lastInsertId();
        $allScores = $game->getScoreboard()->getAllScores();

        foreach ($allScores as $pair) {
            $gamePlayerStmt = Database::getPDO()->prepare("
                INSERT INTO game_player (
                    game_id
                    , player_id
                    , score
                )
                VALUES (
                    :game_id
                    , :player_id
                    , :score
                )
            ");

            $gamePlayerStmt->execute([
                "game_id" => $newGameId,
                "player_id" => $pair->player->getId(),
                "score" => $pair->score
            ]);
        }

        return true;
    }
}
