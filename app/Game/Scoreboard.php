<?php

class Scoreboard
{
    private $scoreboard;

    public function __construct()
    {
        $this->scoreboard = [];
    }
    
    public function addScore(Player $player, int $score) : Scoreboard
    {
        $newPair = new stdClass();

        $newPair->player = $player;
        $newPair->score = $score;

        $playerExists = false;
        foreach ($this->scoreboard as $pair) {
            if ($pair->player->getId() === $player->getId()) {
                $pair->score = $score;
                $playerExists = true;
            }
        }

        if (!$playerExists) {
            $this->scoreboard[] = $newPair;
        }

        return $this;
    }

    public function getWinner()
    {
        if (count($this->scoreboard) === 0) {
            return null;
        }

        $winningPair = $this->scoreboard[0];

        foreach ($this->scoreboard as $pair) {
            if ($pair->score > $winningPair->score) {
                $winningPair = $pair;
            }
        }

        return $winningPair;
    }

    public function getPlayers() : array
    {
        return array_map(function ($pair) {
            return $pair->player;
        }, $this->scoreboard);
    }

    public function getScore(Player $player)
    {
        foreach ($this->scoreboard as $pair) {
            if ($pair->player->getId() === $player->getId()) {
                return $pair->score;
            }
        }

        return null;
    }

    public function getAllScores() : array
    {
        return $this->scoreboard;
    }

    public static function validateScores(int $score1, int $score2) : array {
        $errors = [];

        if ($score1 < 11 && $score2 < 11) {
            $errors[] = "Score needs to be at least 11";
        }

        if ($score1 === $score2) {
            $errors[] = "Scores are tied";
        }

        if ($score1 > $score2 && ($score1 - $score2) < 2) {
            $errors[] = "Need to win by at least 2 points";
        }

        if ($score2 > $score1 && ($score2 - $score1) < 2) {
            $errors[] = "Need to win by at least 2 points";
        }

        return $errors;
    }

    public function __toString() : string {
        $str = "";

        foreach ($this->scoreboard as $pair) {
            $str .= $pair->player->getName() . ": " . $pair->score . PHP_EOL;
        }

        return $str;
    }
}
