<?php

class Helper
{
    static function isHexColor(string $str) : bool
    {
        return preg_match("/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/", $str) === 1;
    }

    static function toLocal(DateTime $utc) : DateTime
    {
        $utc = clone $utc;

        return $utc->setTimezone(new DateTimeZone($_ENV["TIMEZONE"]));
    }

    static function usePhpRouteExt() : bool
    {
        return strtolower($_ENV["USE_PHP_ROUTE_EXT"]) === "yes";
    }

    static function redirectTo($pageName)
    {
        header("Location: {$pageName}" . (self::usePhpRouteExt() ? ".php" : ""));
        die();
    }

    static function link(string $url) : string {
        return "/" . $url . (self::usePhpRouteExt() ? ".php" : "");
    }
}
