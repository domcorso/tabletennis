<?php

class Player
{
    private $id = null;
    private $name = null;
    private $color = null;
    private $createdUtc = null;

    public function __construct()
    {
        
    }

    public static function fromDataSource(array $data) : Player
    {
        $player = new Player();

        $player->setId((int) $data["player_id"]);
        $player->setName((string) $data["name"]);
        $player->setColor((string) $data["color"]);
        $player->setCreatedUtc(new DateTime($data["created_utc"], new DateTimeZone("UTC")));

        return $player;
    }

    protected function setId(int $id) : Player
    {
        $this->id = $id;

        return $this;
    }

    public function setName(string $name) : Player
    {
        $this->name = $name;
        
        return $this;
    }
    
    public function setColor(string $color) : Player
    {
        if (Helper::isHexColor($color)) {
            $this->color = $color;
        }
        
        return $this;
    }

    protected function setCreatedUtc(DateTime $createdUtc) : Player
    {
        $this->createdUtc = $createdUtc;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName($specialChars = false)
    {
        if ($specialChars) {
            return htmlspecialchars($this->name);
        }

        return $this->name;
    }
    
    public function getColor($specialChars = false)
    {
        if ($specialChars) {
            return htmlspecialchars($this->name);
        }

        return $this->color;
    }

    public function getCreatedUtc()
    {
        return $this->createdUtc;
    }

    public function getGames()
    {
        return PlayerDAL::getGames($this);
    }
}
