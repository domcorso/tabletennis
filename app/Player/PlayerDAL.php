<?php

class PlayerDAL extends AbstractDAL
{
    public static function getPlayers(array $filters = []) : array
    {
        $statement = Database::getPDO()->prepare("
            SELECT p.player_id
                , p.name
                , p.color
                , p.created_utc
            FROM player p
            WHERE 1 = 1
                AND (:player_id IS NULL OR :player_id = p.player_id)
            ORDER BY p.name ASC
        ");

        $players = [];
        $statement->execute(parent::normalizeFilters([
            "player_id"
        ], $filters));

        while (($row = $statement->fetch()) !== false) {
            $players[] = Player::fromDataSource($row);
        }

        return $players;
    }

    public static function getPlayerById(int $playerId)
    {
        $players = self::getPlayers([
            "player_id" => $playerId
        ]);

        if (count($players) === 1) {
            return $players[0];
        }

        return null;
    }

    public static function getGames(Player $player) : array
    {
        if ($player->getId() === null) {
            return [];
        }

        $statement = Database::getPDO()->prepare("
            SELECT g.game_id
                , g.created_utc
            FROM game g
            INNER JOIN game_player gp ON g.game_id = gp.game_id
            WHERE gp.player_id = :player_id
            ORDER BY g.created_utc DESC
        ");

        $games = [];
        $statement->execute([
            "player_id" => $player->getId()
        ]);
        
        while (($row = $statement->fetch()) !== false) {
            $games[] = Game::fromDataSource($row);
        }

        return $games;
    }
}
