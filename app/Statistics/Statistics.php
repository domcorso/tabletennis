<?php

class Statistics
{
    private function __construct()
    {

    }

    /**
     * Gets the $count amount of previous games between 2 players
     * 
     * @return Game[]
     */
    public static function getRivarly(Player $p1, Player $p2, int $count = 5) : array
    {
        $previousGames = [];
        
        // Sets variables needed for the query that follows
        Database::getPDO()->prepare("
            SET @player1_id = :player1_id;
            SET @player2_id = :player2_id;
        ")->execute([
            "player1_id" => $p1->getId(),
            "player2_id" => $p2->getId()
        ]);

        $statement = Database::getPDO()->prepare("
            SELECT g.game_id AS game_id
                ,g.created_utc AS created_utc
            FROM game_player gp1
            INNER JOIN game_player gp2 ON gp1.game_id = gp2.game_id AND gp1.player_id != gp2.player_id
            INNER JOIN game g ON gp1.game_id = g.game_id
            WHERE (
                gp1.player_id = @player1_id AND gp2.player_id = @player2_id
            ) OR (
                gp1.player_id = @player2_id AND gp2.player_id = @player1_id
            )
            GROUP BY gp1.game_id
            ORDER BY g.created_utc DESC
            LIMIT :count
        ");

        $statement->bindParam(":count", $count, PDO::PARAM_INT);
        $statement->execute();

        $games = [];
        while (($row = $statement->fetch()) !== false) {
            $games[] = Game::fromDataSource($row);
        }

        return $games;
    }

    public static function getGameHistory(int $playerId, int $count = 5) : array
    {
        $statement = Database::getPDO()->prepare("
            SELECT g.game_id
                ,g.created_utc AS game_created_utc
                ,gp1.score AS score
                ,gp2.score AS opponent_score
                ,p.player_id AS opponent_player_id
                ,p.name AS opponent_name
                ,IF(gp1.score > gp2.score, 'W', 'L') AS result
            FROM game_player gp1
            INNER JOIN game_player gp2 ON gp1.game_id = gp2.game_id AND gp2.player_id != gp1.player_id
            INNER JOIN player p ON gp2.player_id = p.player_id
            INNER JOIN game g ON gp1.game_id = g.game_id
            WHERE gp1.player_id = :player_id
            LIMIT :count
        ");

        $statement->bindParam(":player_id", $playerId, PDO::PARAM_INT);
        $statement->bindParam(":count", $count, PDO::PARAM_INT);
        $statement->execute();

        $history = [];
        while (($row = $statement->fetch()) !== false) {
            $history[] = $row;
        }

        return $history;
    }

    /**
     * Predicts the score of the next game between two players
     */
    public static function predictScoreboard(Player $p1, Player $p2) : Scoreboard {
        $scoreboard = new Scoreboard();
        $prevGames = self::getRivarly($p1, $p2, 7);

        if (count($prevGames) === 0) {
            throw new NonFatalClientException("No game history between the two players.");
        }

        // Find total wins for Player 1
        $p1Wins = array_reduce($prevGames, function(int $count, Game $g) use ($p1) {
            return $count + ($g->getScoreboard()->getWinner()->player->getId() === $p1->getId()) ? 1 : 0;
        }, 0);

        // Find total wins for Player 2
        $p2Wins = array_reduce($prevGames, function(int $count, Game $g) use ($p2) {
            return $count + ($g->getScoreboard()->getWinner()->player->getId() === $p2->getId()) ? 1 : 0;
        }, 0);

        // Who's going to win and lose the next game?
        $nextWinner = $p1Wins > $p2Wins ? $p1 : $p2;
        $nextLoser = $nextWinner === $p1 ? $p2 : $p1;

        // How many points did the loser gain against the winner in their previous encounters?
        $loserTotalPointsGained = array_reduce($prevGames, function(int $count, Game $g) use ($nextLoser) {
            return $count + ($g->getScoreboard()->getScore($nextLoser));
        }, 0);

        // Average points against the winner in their previous encounters
        $loserAveragePoints = round($loserTotalPointsGained / count($prevGames));

        // Determine scores
        $winnerScore = 11;
        $loserScore = $loserAveragePoints > 9 ? 9 : $loserAveragePoints;

        // Add to scoreboard
        $scoreboard->addScore($nextWinner, $winnerScore);
        $scoreboard->addScore($nextLoser, $loserScore);

        return $scoreboard;
    }
}
