<?php

require_once realpath(__DIR__ . "/Application.php");
require_once realpath(__DIR__ . "/NonFatalClientException.php");
require_once realpath(__DIR__ . "/Database.php");
require_once realpath(__DIR__ . "/Helper.php");
require_once realpath(__DIR__ . "/AbstractDAL.php");
require_once realpath(__DIR__ . "/Player/Player.php");
require_once realpath(__DIR__ . "/Player/PlayerDAL.php");
require_once realpath(__DIR__ . "/Game/Game.php");
require_once realpath(__DIR__ . "/Game/Scoreboard.php");
require_once realpath(__DIR__ . "/Game/GameDAL.php");
require_once realpath(__DIR__ . "/Statistics/Statistics.php");

// Load .env into $_ENV
$env_contents = file_get_contents(realpath(__DIR__ . "/../.env"));
foreach (explode("\n", $env_contents) as $line) {
    if (strlen(trim($line)) === 0) continue;

    $pair = explode("=", $line);

    if (count($pair) > 0) {
        $key = trim($pair[0]);
        $value = trim(count($pair) === 2 ? $pair[1] : "");
        $_ENV[$key] = $value;
    }
}

session_start();

$application = new Application();
