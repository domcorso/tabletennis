<?php

require_once realpath(__DIR__ . "/../../../app/bootstrap.php");

header("Content-Type: application/json");
$application->requiresLogin(true);

if (!isset($_GET["ids"])) {
    $application->dieWith(400);
}

$ids = explode(",", $_GET["ids"]);
$ids = array_filter($ids, function ($id) {
    return is_numeric($id);
});

$players = [];

foreach ($ids as $id) {
    $id = (int) $id;
    $player = PlayerDAL::getPlayerById($id);

    if ($player) {
        $players[] = $player;
    }
}

$players = array_map(function ($playerObject) {
    return [
        "id" => $playerObject->getId(),
        "name" => $playerObject->getName(true)
    ];
}, $players);

echo json_encode($players);

?>
