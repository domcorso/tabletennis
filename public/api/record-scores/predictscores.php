<?php

require_once realpath(__DIR__ . "/../../../app/bootstrap.php");

header("Content-Type: application/json");
$application->requiresLogin(true);

if (!isset($_GET["ids"])) {
    $application->dieWith(400);
}

$ids = explode(",", $_GET["ids"]);
$ids = array_filter($ids, function ($id) {
    return is_numeric($id);
});

if (count($ids) !== 2) {
    $application->dieWith(400);
}

$player1 = PlayerDAL::getPlayerById($ids[0]);
$player2 = PlayerDAL::getPlayerById($ids[1]);

if (is_null($player1) || is_null($player2)) {
    $application->dieWith(400);
}

try {
    $scoreboard = Statistics::predictScoreboard($player1, $player2);
} catch (NonFatalClientException $e) {
    echo json_encode([
        "errors" => [$e->getMessage()]
    ]);
    
    $application->dieWith(422);
}

echo json_encode([
    $player1->getId() => $scoreboard->getScore($player1),
    $player2->getId() => $scoreboard->getScore($player2)
]);
