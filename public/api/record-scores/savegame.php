<?php

require_once realpath(__DIR__ . "/../../../app/bootstrap.php");

header("Content-Type: application/json");
$application->requiresLogin(true);

if (
    !isset($_POST["player1_id"]) || !is_numeric($_POST["player1_id"]) ||
    !isset($_POST["player2_id"]) || !is_numeric($_POST["player2_id"]) ||
    !isset($_POST["player1_score"]) || !is_numeric($_POST["player1_score"]) ||
    !isset($_POST["player2_score"]) || !is_numeric($_POST["player2_score"])
) {
    $application->dieWith(400);
}

$player1 = PlayerDAL::getPlayerById((int) $_POST["player1_id"]);
$player2 = PlayerDAL::getPlayerById((int) $_POST["player2_id"]);
$player1_score = (int) $_POST["player1_score"];
$player2_score = (int) $_POST["player2_score"];

if ($player1 === null || $player2 === null) {
    echo json_encode([
        "errors" => [
            "One or more players don't exist in the system"
        ]
    ]);

    $application->dieWith(422);
}

$validationErrors = Scoreboard::validateScores($player1_score, $player2_score);

if (count($validationErrors) > 0) {
    echo json_encode([
        "errors" => $validationErrors
    ]);

    $application->dieWith(422);
}

$game = new Game();
$scoreboard = $game->getScoreboard();
$scoreboard->addScore($player1, $player1_score);
$scoreboard->addScore($player2, $player2_score);

GameDAL::insert($game);

echo json_encode([
    "success" => true
]);

?>
