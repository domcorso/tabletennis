<?php

require_once realpath(__DIR__ . "/../app/bootstrap.php");

$application->requiresLogin();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once realpath(__DIR__ . "/include/head.php"); ?>
    <title>Dashboard - Table Tennis</title>
</head>
<body>
    <div class="body-container">
        
    </div>
    <?php require_once realpath(__DIR__ . "/include/navigation.php"); ?>
    <?php require_once realpath(__DIR__ . "/include/scripts.php"); ?>
</body>
</html>
