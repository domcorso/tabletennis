<?php if ($application->getNavigationVisibility() === true) { ?>
    <div id="nav" class="navigation">
        <a href="<?= Helper::link('dashboard') ?>" class="navigation__link">
            <span>Dashboard</span>
            <span class="material-icons">dashboard</span>
        </a>
        <a href="<?= Helper::link('record-scores') ?>" class="navigation__link">
            <span>Record Scores</span>
            <span class="material-icons">fiber_smart_record</span>
        </a>
        <a href="<?= Helper::link('players') ?>" class="navigation__link">
            <span>Players</span>
            <span class="material-icons">people</span>
        </a>
        <a href="<?= Helper::link('logout') ?>" class="navigation__link">
            <span>Logout</span>
            <span class="material-icons">exit_to_app</span>
        </a>
        <div class="navigation__footer">Table Tennis</div>
    </div>
<?php } ?>
<div class="header">
    <?php if ($application->getNavigationVisibility() === true) { ?>
        <button type="button" id="btnToggleNav" class="header__button material-icons">menu</button>
    <?php } ?>
</div> 
