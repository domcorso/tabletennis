<?php

require_once realpath(__DIR__ . "/../app/bootstrap.php");

if ($application->isLoggedIn()) {
    Helper::redirectTo("dashboard");
} else {
    Helper::redirectTo("login");
}

?>
