const Main = {
    e: {
        btnToggleNav: null,
        nav: null
    },

    init() {
        this.getElements();
        this.bindEvents();
        this.initMultiSelect();
    },

    getElements() {
        this.e.btnToggleNav = document.getElementById("btnToggleNav");
        this.e.nav = document.getElementById("nav");
    },

    bindEvents() {
        // Navigation events
        if (this.e.btnToggleNav && this.e.nav) {
            this.e.btnToggleNav.addEventListener("click", () => {
                this.e.nav.classList.toggle("nav--open");
            });

            document.addEventListener("click", e => {
                if (e.target !== this.e.nav && e.target !== this.e.btnToggleNav && !this.e.nav.contains(e.target) && this.e.nav.classList.contains("nav--open")) {
                    this.e.nav.classList.remove("nav--open");
                }
            });
        }
    },

    switch(name, group) {
        const element = document.querySelector(`[data-switchable-name='${name}'][data-switchable-group='${group}']`);
        const othersInGroup = document.querySelectorAll(`[data-switchable-group='${group}']`);

        othersInGroup.forEach(elm => elm.classList.remove("switchable--active"));
        element.classList.add("switchable--active");
    },

    initMultiSelect() {
        document.addEventListener("click", e => {
            if (!e.target.classList.contains("multiselect__item")) {
                return;
            }

            const item = e.target;
            const multiselectContainer = item.parentElement;
            const maxSelectable = multiselectContainer.dataset.max || Infinity;
            const currentlySelectedItems = multiselectContainer.querySelectorAll(".multiselect__item--selected");

            if (currentlySelectedItems.length >= maxSelectable && !item.classList.contains("multiselect__item--selected")) {
                return;
            }

            item.classList.toggle("multiselect__item--selected");
            multiselectContainer.dispatchEvent(new Event("multiSelect_afterSelected"))
        });
    },

    handleError(error) {
        alert(`Error: ${error.message}`);
    }
};

document.addEventListener("DOMContentLoaded", Main.init.bind(Main));
