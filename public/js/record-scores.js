const RecordScores = {
    e: {
        selectPlayers: {
            msSelectPlayers: null,
            btnContinue: null
        },
        recordScores: {
            rsPlayer1: null,
            rsPlayer2: null,
            btnSaveGame: null,
            btnPredictScores: null
        }
    },

    inProgress: {
        player1: {
            id: null,
            name: null
        },
        player2: {
            id: null,
            name: null
        }
    },

    init() {
        this.getElements();
        this.bindEvents();
    },

    getElements() {
        this.e.selectPlayers.msSelectPlayers = document.getElementById("msSelectPlayers");
        this.e.selectPlayers.btnContinue = document.getElementById("btnContinue");
        this.e.recordScores.rsPlayer1 = document.getElementById("rsPlayer1");
        this.e.recordScores.rsPlayer2 = document.getElementById("rsPlayer2");
        this.e.recordScores.btnSaveGame = document.getElementById("btnSaveGame");
        this.e.recordScores.btnPredictScores = document.getElementById("btnPredictScores");
    },

    bindEvents() {
        this.e.selectPlayers.msSelectPlayers.addEventListener("multiSelect_afterSelected", () => {
            const numSelectedItems = this.e.selectPlayers.msSelectPlayers.querySelectorAll(".multiselect__item--selected").length;

            if (numSelectedItems === 2) {
                this.e.selectPlayers.btnContinue.disabled = false;
            } else {
                this.e.selectPlayers.btnContinue.disabled = true;
            }
        }, true);

        this.e.selectPlayers.btnContinue.addEventListener("click", () => {
            const selectedItems = this.e.selectPlayers.msSelectPlayers.querySelectorAll(".multiselect__item--selected");
            this.startInProgress(selectedItems[0].dataset.playerId, selectedItems[1].dataset.playerId);
        });

        this.e.recordScores.btnSaveGame.addEventListener("click", () => {
            this.saveGame();
        });

        this.e.recordScores.btnPredictScores.addEventListener("click", () => {
            this.predictScores();
        });

        document.addEventListener("click", e => {
            if (e.target.classList.contains("button--auto-win")) {
                const input = e.target.parentElement.querySelector(".recordscores__input");
                input.value = 11;
            }
        });
    },

    startInProgress(player1_id, player2_id) {
        fetch(`/api/record-scores/playerinfo?ids=${player1_id},${player2_id}`, {
            credentials: "same-origin"
        }).then(response => {
            return response.json();
        }).then(players => {
            if (players.length !== 2) {
                throw new Error("2 Players Not Returned");
            }

            this.inProgress.player1 = players[0];
            this.inProgress.player2 = players[1];
            this.showInProgressView();
        }).catch(error => {
            Main.handleError(error);
        });
    },

    saveGame() {
        const requestBody = new FormData();

        requestBody.append("player1_id", this.inProgress.player1.id);
        requestBody.append("player2_id", this.inProgress.player2.id);
        requestBody.append("player1_score", this.e.recordScores.rsPlayer1.querySelector(".recordscores__input").value);
        requestBody.append("player2_score", this.e.recordScores.rsPlayer2.querySelector(".recordscores__input").value);

        // Keep track of the status code
        let statusCode = null;

        fetch("/api/record-scores/savegame", {
            method: "post",
            credentials: "same-origin",
            body: requestBody
        }).then(response => {
            // Bad parameters (unlikely)
            if (response.status === 400) {
                throw new Error("Something went wrong. Please try again.");
            }

            statusCode = response.status;

            return response.json();
        }).then(data => {
            // Validation error on scores
            if (statusCode === 422) {
                throw new Error(data.errors.join(", "));
            }

            // All good
            this.showSelectPlayersView();
        }).catch(error => {
            Main.handleError(error);
        });
    },

    predictScores() {
        // Keep track of the status code
        let statusCode = null;

        fetch(`/api/record-scores/predictscores?ids=${this.inProgress.player1.id},${this.inProgress.player2.id}`, {
            method: "get",
            credentials: "same-origin"
        }).then(response => {
            // Bad parameters (unlikely)
            if (response.status === 400) {
                throw new Error("Something went wrong. Please try again.");
            }

            statusCode = response.status;

            return response.json();
        }).then(data => {
            // Validation error on scores
            if (statusCode === 422) {
                throw new Error(data.errors.join(", "));
            }

            // All good
            this.e.recordScores.rsPlayer1.querySelector(".recordscores__input").value = data[this.inProgress.player1.id];
            this.e.recordScores.rsPlayer2.querySelector(".recordscores__input").value = data[this.inProgress.player2.id];
        }).catch(error => {
            Main.handleError(error);
        });
    },

    resetInProgressView() {
        document.querySelectorAll(".recordscores__name").forEach(elm => elm.textContent = "Nobody");
        document.querySelectorAll(".recordscores__input").forEach(elm => elm.value = "");
    },

    showInProgressView() {
        this.resetInProgressView();
        
        this.e.recordScores.rsPlayer1.querySelector(".recordscores__name").textContent = this.inProgress.player1.name;
        this.e.recordScores.rsPlayer2.querySelector(".recordscores__name").textContent = this.inProgress.player2.name;

        Main.switch("gameInProgress", "main");
    },

    resetSelectPlayersView() {
        document.querySelectorAll(".multiselect__item").forEach(elm => elm.classList.remove("multiselect__item--selected"));
    },

    showSelectPlayersView() {
        this.resetSelectPlayersView();
        Main.switch("selectPlayers", "main");
    }
};

document.addEventListener("DOMContentLoaded", RecordScores.init.bind(RecordScores));
