<?php

require_once realpath(__DIR__ . "/../app/bootstrap.php");

if (isset($_POST["tt_username"]) && isset($_POST["tt_password"])) {
    $loginResult = $application->login($_POST["tt_username"], $_POST["tt_password"]);

    if ($loginResult === true) {
        Helper::redirectTo($application->getDefaultPage());
    } else {
        $errorMessage = "Incorrect username and/or password.";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once realpath(__DIR__ . "/include/head.php"); ?>
    <link rel="stylesheet" href="/css/login.css">
    <title>Login - Table Tennis</title>
</head>
<body>
    <div class="login">
        <?php if (isset($errorMessage)) { ?>
            <div class="banner banner--error"><?= $errorMessage ?></div>
        <?php } ?>
        <img src="/images/paddle.png" alt="Table Tennis" class="login__logo">
        <form class="form login__form" action="<?= Helper::link('login') ?>" method="post">
            <input type="text" name="tt_username" placeholder="Username">
            <input type="password" name="tt_password" placeholder="Password">
            <button class="button button--full-width" type="submit">Login</button>
        </form>
    </div>
    <?php require_once realpath(__DIR__ . "/include/scripts.php"); ?>
</body>
</html>
