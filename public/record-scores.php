<?php

require_once realpath(__DIR__ . "/../app/bootstrap.php");

$application->requiresLogin();

$players = PlayerDAL::getPlayers();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once realpath(__DIR__ . "/include/head.php"); ?>
    <link rel="stylesheet" href="<?= $application->resource('/css/record-scores.css') ?>">
    <title>Record Scores - Table Tennis</title>
</head>
<body>
    <div class="body-container">
        <!-- Switchable: Select Players -->
        <div class="switchable switchable--active" data-switchable-name="selectPlayers" data-switchable-group="main">
            <h2>Select Players</h2>
            <div id="msSelectPlayers" class="multiselect" data-max="2">
                <?php foreach ($players as $player) { ?>
                    <div class="multiselect__item" data-player-id="<?= $player->getId() ?>">
                        <?= $player->getName(true) ?>
                    </div>
                <?php } ?>
            </div>
            <button id="btnContinue" disabled class="button button--floating-submit button--success">Continue</button>
        </div>
        <!-- Switchable: Game In Progress -->
        <div class="switchable" data-switchable-name="gameInProgress" data-switchable-group="main">
            <div class="recordscores__container">
                <div class="recordscores" id="rsPlayer1">
                    <div class="recordscores__name">Nobody</div>
                    <input class="recordscores__input" type="number">
                    <button class="button button--full-width u-no-border-radius button--auto-win">11</button>
                </div>
                <div class="recordscores" id="rsPlayer2">
                    <div class="recordscores__name">Nobody</div>
                    <input class="recordscores__input" type="number">
                    <button class="button button--secondary button--full-width u-no-border-radius button--auto-win">11</button>
                </div>
            </div>
            <button id="btnPredictScores" class="button button--full-width button--success">Predict Scores</button>
            <button id="btnSaveGame" class="button button--floating-submit button--success">Save Game</button>
        </div>
    </div>
    <?php require_once realpath(__DIR__ . "/include/navigation.php"); ?>
    <?php require_once realpath(__DIR__ . "/include/scripts.php"); ?>
    <script src="<?= $application->resource('/js/record-scores.js') ?>"></script>
</body>
</html>
